package cl.duoc.mantenedorejemploprueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnAgregar, btnEliminar, btnListar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        btnListar = (Button) findViewById(R.id.btnListar);

        btnAgregar.setOnClickListener(this);
        btnEliminar.setOnClickListener(this);
        btnListar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnAgregar:
                Intent i = new Intent(this, AgregarEquipoActivity.class);
                startActivity(i);
                break;
            case R.id.btnEliminar:
                Intent e = new Intent(this, BuscarYEliminarActivity.class);
                startActivity(e);
                break;

            case R.id.btnListar:
                Intent o = new Intent(this, ListarEquiposActivity.class);
                startActivity(o);
                break;
        }
    }
}
