package cl.duoc.mantenedorejemploprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AgregarEquipoActivity extends AppCompatActivity {

    private EditText txtNombre, txtFecha, txtComuna;
    private Button btnAgregar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_equipo);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtFecha = (EditText)findViewById(R.id.txtFecha);
        txtComuna = (EditText)findViewById(R.id.txtComuna);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                agregarEquipo();
            }
        });
    }

    private void agregarEquipo() {

        boolean validaEquipo = true;
        if(txtNombre.getText().toString().length()<1){
            txtNombre.setError("Ingrese Nombre");
            validaEquipo = false;
        }

        if(txtFecha.getText().toString().length()<1){
            txtFecha.setError("Ingrese Fecha");
            validaEquipo = false;
        }

        if(txtComuna.getText().toString().length()<1){
            txtComuna.setError("Ingrese Comuna");
            validaEquipo = false;
        }

        if(validaEquipo){
            Equipo e = new Equipo();
            e.setNombre(txtNombre.getText().toString());
            e.setFechaCreacion(txtFecha.getText().toString());
            e.setComuna(txtComuna.getText().toString());

            if(BaseDeDatos.isAgregarEquipo(e)){
                Toast.makeText(AgregarEquipoActivity.this, "Equipo creado correctamente", Toast.LENGTH_LONG).show();
                limpiaCajasDeTexto();
            }else{
                Toast.makeText(AgregarEquipoActivity.this, "El equipo ya existe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void limpiaCajasDeTexto() {
        txtFecha.setText("");
        txtNombre.setText("");
        txtComuna.setText("");
    }


}
