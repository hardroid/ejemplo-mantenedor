package cl.duoc.mantenedorejemploprueba;

/**
 * Created by Duoc on 02-09-2016.
 */
public class Equipo {
    private String nombre;
    private String fechaCreacion;
    private String comuna;

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getComuna() {
        return comuna;
    }

    public void setComuna(String comuna) {
        this.comuna = comuna;
    }
}
