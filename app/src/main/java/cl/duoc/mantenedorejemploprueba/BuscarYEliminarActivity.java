package cl.duoc.mantenedorejemploprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BuscarYEliminarActivity extends AppCompatActivity {

    private Button btnBuscar, btnEliminar;
    private EditText txtBuscarNombre;
    private TextView txtMostrarEquipo;
    private Equipo equipoEncontrado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_yeliminar);
        btnBuscar = (Button)findViewById(R.id.btnBuscar);
        btnEliminar = (Button)findViewById(R.id.btnEliminar);
        txtBuscarNombre = (EditText)findViewById(R.id.txtBuscarNombre);
        txtMostrarEquipo = (TextView)findViewById(R.id.txtMostrarEquipo);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buscarEquipo();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(equipoEncontrado != null){
                    if(BaseDeDatos.eliminarEquipo(equipoEncontrado.getNombre())){
                        Toast.makeText(BuscarYEliminarActivity.this, "Equipo eliminado", Toast.LENGTH_SHORT).show();
                        limpiarResultados();
                    }else{
                        Toast.makeText(BuscarYEliminarActivity.this, "Equipo no eliminado", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void buscarEquipo() {
        if(txtBuscarNombre.getText().toString().length() < 1){
            txtBuscarNombre.setError("Ingrese nombre a Buscar");
            limpiarResultados();
        }else{
            equipoEncontrado = BaseDeDatos.buscarEquipo(txtBuscarNombre.getText().toString());
            if(equipoEncontrado == null){
                Toast.makeText(BuscarYEliminarActivity.this, "El equipo no se encontro", Toast.LENGTH_SHORT).show();
                limpiarResultados();
            }else{
                txtMostrarEquipo.setText("Nombre: " + equipoEncontrado.getNombre() + "\n" +
                "Fecha Creación: " + equipoEncontrado.getFechaCreacion() + "\n" +
                "Comuna: " + equipoEncontrado.getComuna());
                txtMostrarEquipo.setVisibility(View.VISIBLE);
                btnEliminar.setVisibility(View.VISIBLE);
            }
        }
    }

    private void limpiarResultados() {
        txtMostrarEquipo.setText("");
        txtMostrarEquipo.setVisibility(View.INVISIBLE);
        btnEliminar.setVisibility(View.INVISIBLE);
    }


}
