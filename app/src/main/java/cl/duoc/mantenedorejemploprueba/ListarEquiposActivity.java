package cl.duoc.mantenedorejemploprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class ListarEquiposActivity extends AppCompatActivity {

    private ListView lvEquipos;
    private EquiposAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_equipos);

        lvEquipos = (ListView)findViewById(R.id.lvEquipos);
        adapter = new EquiposAdapter(this, BaseDeDatos.listarEquipos());
        lvEquipos.setAdapter(adapter);
    }

}
