package cl.duoc.mantenedorejemploprueba;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Duoc on 02-09-2016.
 */
public class EquiposAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Equipo> dataSource;

    public EquiposAdapter(Context context, ArrayList<Equipo> dataSource) {
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.equipo_item, parent, false);
        }

        // Set data into the view.
        TextView txtNombre = (TextView) rowView.findViewById(R.id.txtTitulo);
        TextView txtFecha = (TextView) rowView.findViewById(R.id.txtFecha);
        TextView txtComuna = (TextView) rowView.findViewById(R.id.txtComuna);

        Equipo item = this.dataSource.get(position);
        txtNombre.setText(item.getNombre());
        txtFecha.setText(item.getFechaCreacion());
        txtComuna.setText(item.getComuna());

        return rowView;
    }
}
