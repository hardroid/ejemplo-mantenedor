package cl.duoc.mantenedorejemploprueba;

import java.util.ArrayList;

/**
 * Created by Duoc on 02-09-2016.
 */
public class BaseDeDatos {
    private static ArrayList<Equipo> misDatos = new ArrayList<>();

    public static boolean isAgregarEquipo(Equipo nuevoEquipo) {
        if (buscarEquipo(nuevoEquipo.getNombre()) == null) {
            misDatos.add(nuevoEquipo);
            return true;
        } else {
            return false;
        }
    }

    public static Equipo buscarEquipo(String nombre) {
        Equipo retorno = null;
        for (Equipo aux : misDatos) {
            if (aux.getNombre().equals(nombre)) {
                retorno = aux;
                break;
            }
        }
        return retorno;
    }

    public static boolean eliminarEquipo(String nombre){
        for (int x = 0; x < misDatos.size(); x++) {
            if (misDatos.get(x).getNombre().equals(nombre)) {
                misDatos.remove(x);
                return true;
            }
        }
        return false;
    }

    public static ArrayList<Equipo> listarEquipos(){
        return misDatos;
    }


}
